﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode()]
public class ProgressBar : MonoBehaviour
{
    #region Properties
    /// <summary>
    /// Current value
    /// </summary>
    public float Value = 0;
    /// <summary>
    /// Maximum progress value (for mapping value)
    /// </summary>
    public float Maximum = 1;
    /// <summary>
    /// Minimum progress value (for mapping value)
    /// </summary>
    public float Minimum = 0;

    public Image FillImage;
    public Color FillColor;
    #endregion


    #region Unity Methods
    protected virtual void Update()
    {
        UpdateFill();
    }
    #endregion


    #region Custom Methods
    public void SetProgress(float value)
    {
        Value = value;
    }

    private void UpdateFill()
    {
        // Allow for custom min/max calculations (against target)
        float currentOffset = Value - Minimum;
        float maximumOffset = Maximum - Minimum;

        float fillAmount = currentOffset / maximumOffset;

        if (FillImage != null)
        {
            FillImage.fillAmount = fillAmount;
            FillImage.color = FillColor;
        }
    }
    #endregion
}
