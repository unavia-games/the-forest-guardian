﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode()]
public class ProgressBarIcon : ProgressBar
{
    #region Properties
    public Image IconImage;
    public Sprite IconSprite;
    #endregion


    #region Unity Methods
    protected override void Update()
    {
        base.Update();

        UpdateIcon();
    }
    #endregion


    #region Custom Methods
    private void UpdateIcon()
    {
        if (IconImage != null)
        {
            IconImage.color = FillColor;
            IconImage.sprite = IconSprite;
        }
    }
    #endregion
}
