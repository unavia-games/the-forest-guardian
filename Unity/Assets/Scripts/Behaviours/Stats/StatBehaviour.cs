﻿using System;
using UnityEngine;

namespace Unavia.TheForestGuardian.Behaviours
{
    /// <summary>
    /// Base stat behaviour
    /// </summary>
    public abstract class StatBehaviour : MonoBehaviour
    {
        #region Properties
        /// <summary>
        /// Base stat value (ie. new/resting state)
        /// </summary>
        [SerializeField]
        [Min(0)]
        protected float baseValue = 100f;
        /// <summary>
        /// Minimum stat value (triggers event on reach/exceed)
        /// </summary>
        [SerializeField]
        [Min(0)]
        protected float minValue = 0f;
        /// <summary>
        /// Maximum stat value (triggers event on reach/exceed)
        /// </summary>
        [SerializeField]
        [Min(0)]
        protected float maxValue = 100f;

        // TODO: Support stat modifiers (maybe support over time as well)???
        //         Taken from: https://forum.unity.com/threads/tutorial-character-stats-aka-attributes-system.504095/

        /// <summary>
        /// Stat value
        /// </summary>
        public float Value { get => baseValue; }
        /// <summary>
        /// Stat maximum value
        /// </summary>
        public float Maximum { get => maxValue; }
        /// <summary>
        /// Stat minimum value
        /// </summary>
        public float Minimum { get => minValue; }
        /// <summary>
        /// Whether the stat is at its min value
        /// </summary>
        public bool IsMin { get => Mathf.Approximately(baseValue, minValue); }
        /// <summary>
        /// Whether the stat is at its max value
        /// </summary>
        public bool IsMax { get => Mathf.Approximately(baseValue, maxValue); }
        /// <summary>
        /// Stat percentage
        /// </summary>
        public float Percentage { get => baseValue / maxValue; }
        #endregion


        #region Events
        /// <summary>
        /// Invoke when the value has changed
        /// </summary>
        [HideInInspector]
        public Action<float, float> OnChange;
        /// <summary>
        /// Invoked when the max value is manually reached/exceeded
        /// </summary>
        [HideInInspector]
        public Action OnMaxValue;
        /// <summary>
        /// Invoked when the minimum value is manually reached/exceeded
        /// </summary>
        [HideInInspector]
        public Action OnMinValue;
        #endregion


        #region Unity Methods
        #endregion


        #region Custom Methods
        /// <summary>
        /// Increase a stat by an amount
        /// </summary>
        /// <param name="value">Amount to increase</param>
        public virtual void Increase(float value)
        {
            baseValue = Mathf.Clamp(baseValue + value, minValue, maxValue);

            OnChange?.Invoke(value, baseValue);

            // Notify listeners when the maximum value is reached/exceeded
            if (Mathf.Approximately(baseValue, maxValue))
                OnMaxValue?.Invoke();
        }

        /// <summary>
        /// Decrease a stat by an amount
        /// </summary>
        /// <param name="value">Amount to decrease</param>
        public virtual void Decrease(float value)
        {
            baseValue = Mathf.Clamp(baseValue - value, minValue, maxValue);

            OnChange?.Invoke(-value, baseValue);

            // Notify listeners when the minimum value is reached/exceeded
            if (Mathf.Approximately(baseValue, minValue))
                OnMaxValue?.Invoke();
        }
        #endregion
    }
}
