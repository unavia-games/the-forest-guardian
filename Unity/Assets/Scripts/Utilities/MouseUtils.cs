﻿using UnityEngine;

public class MouseUtils : MonoBehaviour
{
    #region Properties
    // [Header("Cursor Settings")]
    /// <summary>
    /// Whether to hide the cursor in game mode
    /// </summary>
    private bool HideCursor = true;
    /// <summary>
    /// Whether the cursor is locked to game window (only affects if not hidden)
    /// </summary>
    private bool LockCursor = true;
    #endregion


    #region Unity Methods
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Update the cursor settings
    /// </summary>
    /// <remarks>This is currently broken oftern!</remarks>
    private void UpdateCursor()
    {
        // Handles changes done via the Inspector while playing only!
        if (!Application.isPlaying) return;

        Cursor.visible = !HideCursor;

        if (HideCursor)
            Cursor.lockState = CursorLockMode.Locked;
        else if (LockCursor)
            Cursor.lockState = CursorLockMode.Confined;
        else
            Cursor.lockState = CursorLockMode.None;
    }
    #endregion
}
