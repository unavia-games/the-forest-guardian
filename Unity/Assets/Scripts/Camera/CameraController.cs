﻿using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    #region Properties
    public InputActions Controls;

    public CinemachineFreeLook FreeLookCinemachine;

    [Header("Configuration")]
    /// <summary>
    /// Horizontal look speed
    /// </summary>
    [Range(1f, 5f)]
    public float HorizontalLookSpeed = 2.0f;
    /// <summary>
    /// Vertical look speed
    /// </summary>
    [Range(1f, 5f)]
    public float VerticalLookSpeed = 3.0f;

    [Header("Debug")]
    /// <summary>
    /// Whether looking around requires the right mouse button
    /// </summary>
    public bool LookRequiresKey = false;
    /// <summary>
    /// Whether camera zoom is controlled by look or scroll
    /// </summary>
    public bool UseScrollForZoom = false;
    #endregion

    // Additional multiplier to get the top values in a decent range
    private float xLookSpeedMultiplier = 20f;
    // Correct Y sensitivity depending on rig
    private float yLookSpeedMultiplier = 40f;
    private float yScrollSpeedMultiplier = 5f;

    private float xAxisValue;
    private float yAxisValue;

    #region Unity Methods
    public void Awake()
    {
        Controls = new InputActions();
    }

    public void Start()
    {
        xAxisValue = FreeLookCinemachine.m_XAxis.Value;
        yAxisValue = FreeLookCinemachine.m_YAxis.Value;
    }

    public void OnEnable()
    {
        Controls.Enable();
    }

    public void OnDisable()
    {
        Controls.Disable();
    }

    public void Update()
    {
        HandleLook();
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Handle camera look movement
    /// </summary>
    /// <remarks>
    /// Adapted from https://forum.unity.com/threads/free-look-camera-and-mouse-responsiveness.642886/#post-4316068
    /// </remarks>
    public void HandleLook()
    {
        Vector2 lookDelta = Controls.PlayerMovement.Look.ReadValue<Vector2>();
        Vector2 scrollDelta = Controls.PlayerMovement.Zoom.ReadValue<Vector2>();

        // Player can only look while holding the right mouse button (if enabled)
        bool canLookX = Controls.PlayerMovement.EnableLook.ReadValue<float>() > 0;
        if (canLookX || !LookRequiresKey)
        {
            float mouseX = lookDelta.x * HorizontalLookSpeed * xLookSpeedMultiplier * Time.deltaTime;

            xAxisValue += mouseX;
            FreeLookCinemachine.m_XAxis.Value = xAxisValue;
        }

        // Player can either zoom with the mouse or the scroll wheel
        if (!UseScrollForZoom)
        {
            float mouseY = lookDelta.y * VerticalLookSpeed * yLookSpeedMultiplier * Time.deltaTime;

            // Transform yAxis value into expected range (0-1)
            mouseY /= 360f;

            yAxisValue = Mathf.Clamp01(yAxisValue - mouseY);
            FreeLookCinemachine.m_YAxis.Value = yAxisValue;
        }
        else
        {
            // TODO: Add some smoothing here (with coroutine?)!!!
            float mouseY = scrollDelta.y / (VerticalLookSpeed * yScrollSpeedMultiplier) * Time.deltaTime;

            yAxisValue = Mathf.Clamp01(yAxisValue - mouseY);
            FreeLookCinemachine.m_YAxis.Value = yAxisValue;
        }
    }
    #endregion
}
