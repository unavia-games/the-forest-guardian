﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Unavia.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Unavia.TheForestGuardian.Utilities
{
    /// <summary>
    /// Game debug console/commands
    /// </summary>
    public class DebugConsoleManager : GameSingleton<DebugConsoleManager>
    {
        #region Properties
        [SerializeField]
        private InputAction debugAction;

        public List<DebugCommandBase> Commands { get; private set; }
        #endregion

        /// <summary>
        /// Track the time scale before opening dialog (in case it was already paused)
        /// </summary>
        private float pausedTimeScale;
        private bool showConsole;
        private bool showHelp;
        // Debug console text field input
        private string helpInputText;
        private Vector2 helpScroll;

        private DebugCommandBase lastCommand;
        private DebugCommandResult result = DebugCommandResult.Default;

        // TODO: Possibly move to a "restore" utility in the MouseUtils?
        private CursorLockMode previousLockMode;
        private bool previousCursorShown;


        #region Unity Methods
        void Awake()
        {
            Commands = new List<DebugCommandBase>();

            RegisterSelfCommands();
            RegisterAttachedCommands();
        }


        private void OnEnable()
        {
            debugAction.Enable();
            debugAction.performed += OnToggleDebug;
        }

        private void OnDisable()
        {
            debugAction.Disable();
            debugAction.performed -= OnToggleDebug;
        }

        void Update()
        {
            if (Keyboard.current.enterKey.wasPressedThisFrame && showConsole)
                OnInput();
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Add a debug command
        /// </summary>
        /// <param name="command">New debug command</param>
        public void AddCommand(DebugCommandBase command)
        {
            if (Commands == null)
                // NOTE: Only necessary to allow custom inspector to safely call before awake (ie. not running)!
                Commands = new List<DebugCommandBase>();

            // Adding the same command twice should replace the previous command
            for (int i = 0; i < Commands.Count; i++)
            {
                if (Commands[i].Name == command.Name)
                {
                    Debug.LogWarning($"Overriding default {command.Name} command!");
                    Commands[i] = command;
                    return;
                }
            }

            Commands.Add(command);
        }

        /// <summary>
        /// Register self commands (shared with all projects using the Debug Console)
        /// </summary>
        public void RegisterSelfCommands()
        {
            // Other commands are registered to the singleton instance
            AddCommand(
                new DebugCommand("help", "Show all debug commands", "help",
                () =>
                {
                    showHelp = true;
                    return DebugCommandResult.Default;
                }));
        }

        /// <summary>
        /// Register all commands attached to the singleton instance GameObject
        /// </summary>
        public void RegisterAttachedCommands()
        {
            // Register all command components on this object
            IDebugCommands[] childCommands = GetComponents<IDebugCommands>();
            foreach (IDebugCommands commands in childCommands)
            {
                commands.RegisterCommands(this);
            }
        }

        private void OnInput()
        {
            // Commands should not have any extra spaces (except between arguments)
            string safeString = Regex.Replace(helpInputText.Trim(), " {2,}", " ");
            string[] arguments = safeString.Split(' ');

            bool hasMatch = false;
            helpInputText = "";

            foreach (DebugCommandBase command in Commands)
            {
                if (!safeString.StartsWith(command.Name)) continue;
                lastCommand = command;
                hasMatch = true;

                // Several types of supported commands:
                //   - COMMAND
                //   - COMMAND <bool>
                //   - COMMAND <int>
                //   - COMMAND <string> <int>
                try
                {
                    if (command as DebugCommand != null)
                    {
                        result = (command as DebugCommand).Invoke();
                    }
                    else if (command as DebugCommand<bool> != null)
                    {
                        result = (command as DebugCommand<bool>).Invoke(bool.Parse(arguments[1]));
                    }
                    else if (command as DebugCommand<int> != null)
                    {
                        result = (command as DebugCommand<int>).Invoke(int.Parse(arguments[1]));
                    }
                    else if (command as DebugCommand<string, int> != null)
                    {
                        result = (command as DebugCommand<string, int>).Invoke(arguments[1], int.Parse(arguments[2]));
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
                catch
                {
                    result = DebugCommandResult.InvalidArgs;
                }
            };

            // Help should be hidden after commands are run
            if (lastCommand != null && lastCommand.Name != "help")
                showHelp = false;

            if (!hasMatch)
                result = new DebugCommandResult(false, "No matching command found");
        }

        /// <summary>
        /// Show or hide the debug console
        /// </summary>
        /// <param name="ctx">Triggering action</param>
        private void OnToggleDebug(InputAction.CallbackContext ctx)
        {
            if (!showConsole)
            {
                pausedTimeScale = Time.timeScale;
                Time.timeScale = 0f;

                // Always show/unlock mouse in debug menu
                previousLockMode = Cursor.lockState;
                previousCursorShown = Cursor.visible;
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
            }
            else
            {
                Time.timeScale = pausedTimeScale;

                Cursor.lockState = previousLockMode;
                Cursor.visible = previousCursorShown;
            }

            showConsole = !showConsole;
            showHelp = false;
            helpInputText = "";
        }

        #endregion


        #region GUI
        private void OnGUI()
        {
            if (!showConsole) return;

            float y = Screen.height;
            float debugPanelHeight = 30f;
            float responsePanelHeight = 30f;
            float debugInputHeight = 20f;
            float helpPanelHeight = 200f;

            Color errorColor = new Color(1f, 0.55f, 0.55f, 1f);
            Color successColor = new Color(0.55f, 1f, 0.55f, 1f);

            // Debug console background box
            GUI.backgroundColor = new Color(0, 0, 0, 1f);
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");

            // Debug console input field
            GUI.Box(new Rect(0, y -= debugPanelHeight, Screen.width, debugPanelHeight), "");
            GUI.SetNextControlName("input");
            GUI.backgroundColor = new Color(0, 0, 0, 0);
            helpInputText = GUI.TextField(new Rect(10f, y + 5f, Screen.width - 20f, debugInputHeight), helpInputText);
            GUI.FocusControl("input");

            // Previous command result message
            if (result.HasMessage)
            {
                GUI.backgroundColor = new Color(0, 0, 0, 0.5f);
                GUI.Box(new Rect(0, y -= responsePanelHeight, Screen.width, responsePanelHeight), "");
                GUI.color = result.Success ? successColor : errorColor;
                GUI.Label(new Rect(10f, y + 5, Screen.width, responsePanelHeight), result.Message);
            }

            // Debug console help box
            if (showHelp)
            {
                GUI.color = Color.white;
                GUI.backgroundColor = new Color(0.8f, 1f, 0.8f, 0.7f);
                GUI.Box(new Rect(0, y -= helpPanelHeight, Screen.width, helpPanelHeight), "");

                Rect viewport = new Rect(0, 0, Screen.width - 30, 22 * Commands.Count);
                helpScroll = GUI.BeginScrollView(new Rect(0, y + 5f, Screen.width, helpPanelHeight - 10), helpScroll, viewport);

                List<DebugCommandBase> sortedList = Commands.OrderBy(x => x.Name).ToList();
                for (int i = 0; i < sortedList.Count; i++)
                {
                    DebugCommandBase command = sortedList[i];
                    string label = $"{command.Format} - {command.Description}";
                    Rect labelRect = new Rect(5, 20 * i, viewport.width - 100, 20);
                    GUI.Label(labelRect, label);
                }

                GUI.EndScrollView();
            }
        }
        #endregion
    }
}
