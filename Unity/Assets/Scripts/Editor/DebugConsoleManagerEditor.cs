﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Unavia.TheForestGuardian.Utilities
{
    [CustomEditor(typeof(DebugConsoleManager))]
    public class DebugConsoleManagerEditor : Editor
    {
        private Vector2 scrollPos;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            DebugConsoleManager console = (DebugConsoleManager)target;

            GUILayout.Space(16);
            EditorGUILayout.HelpBox("List of all commands registered with Debug Console", MessageType.Info, true);
            GUILayout.Space(8);

            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Height(500));

            // NOTE: Commands must be manually registered when in Editor mode (since main script is not being run)
            if (!Application.isPlaying)
            {
                if (console.Commands != null)
                    console.Commands.Clear();

                console.RegisterSelfCommands();
                console.RegisterAttachedCommands();
            }

            // Register and draw the help commands in the Inspector
            DrawCommandList(console.Commands);

            EditorGUILayout.EndScrollView();
        }

        /// <summary>
        /// Draw the list of debug commands
        /// </summary>
        /// <param name="commands">List of debug commands</param>
        private void DrawCommandList(List<DebugCommandBase> commands)
        {
            commands.OrderBy(x => x.Name).ToList().ForEach((command) =>
            {
                GUI.color = Color.white;
                GUILayout.Label(command.Format);
                GUI.color = new Color(0.75f, 0.75f, 0.75f, 1f);
                GUILayout.Label(command.Description);
                GUILayout.Space(4);
            });
        }
    }
}
