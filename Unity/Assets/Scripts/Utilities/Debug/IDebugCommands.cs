﻿namespace Unavia.TheForestGuardian.Utilities
{
    /// <summary>
    /// Interface to allow Debug Console to register commands defined in custom classes,
    ///   when attached to the Debug Console singleton GameObject.
    /// </summary>
    public interface IDebugCommands
    {
        /// <summary>
        /// Register debug commands with the debug console singleton
        /// </summary>
        /// <param name="console">Debug console reference</param>
        /// <remarks>
        /// Components with this inteface on the singleton instance will be automatically registered.
        /// </remarks>
        void RegisterCommands(DebugConsoleManager console);
    }
}
