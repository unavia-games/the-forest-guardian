# External Assets

This project uses several external dependencies that must be installed/configured before working on the project. All listed dependencies should be moved into the `External/` directory unless specified.

## Unity Packages

- Cinemachine - Camera management
- Postprocessing - Postprocessing effects
- TextMesh Pro - Improved UI text

## Asset Store Packages

List of all required external assets and links:

- [ALINE](https://assetstore.unity.com/packages/tools/gui/aline-162772) - Debug drawing
- [Easy Character Movement](https://assetstore.unity.com/packages/templates/systems/easy-character-movement-57985) - Character controller
- [Event System - Dispatcher](https://assetstore.unity.com/packages/tools/integration/event-system-dispatcher-12715) - Event messaging system
- [Prefab Painter 2](https://assetstore.unity.com/packages/tools/painting/prefab-painter-2-61331#releases) - Placing prefabs randomly
- [Procedural UI Image](https://assetstore.unity.com/packages/tools/gui/procedural-ui-image-52200) - Procedural UI images

## Custom Packages

- [Simple Utilities](https://gitlab.com/unavia-games/simple-packages/simple-utilities)
