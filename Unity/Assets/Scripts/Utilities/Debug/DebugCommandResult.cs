﻿namespace Unavia.TheForestGuardian.Utilities
{
    /// <summary>
    /// Console debug command result
    /// </summary>
    /// <remarks>Used to indicate command success to user.</remarks>
    public struct DebugCommandResult
    {
        /// <summary>
        /// Whether command succeeded
        /// </summary>
        public bool Success { get; private set; }
        /// <summary>
        /// Command result message
        /// </summary>
        public string Message { get; private set; }
        /// <summary>
        /// Whether the result has a message
        /// </summary>
        public bool HasMessage { get => Message != ""; }

        /// <summary>
        /// Default result (empty success)
        /// </summary>
        static public readonly DebugCommandResult Default = new DebugCommandResult(true, "");
        /// <summary>
        /// Failure result
        /// </summary>
        static public readonly DebugCommandResult Failure = new DebugCommandResult(false, "Invalid or failed command");
        /// <summary>
        /// Invalid args
        /// </summary>
        static public readonly DebugCommandResult InvalidArgs = new DebugCommandResult(false, "Invalid command arguments");

        public DebugCommandResult(bool success, string message)
        {
            Success = success;
            Message = message;
        }
    }
}
