﻿using Unavia.TheForestGuardian.Behaviours;
using UnityEngine;

/// <summary>
/// Stat progress bar
/// </summary>
[RequireComponent(typeof(ProgressBar))]
public class StatProgressBar : MonoBehaviour
{
    #region Properties
    [SerializeField]
    private StatBehaviour stat;
	#endregion

    private ProgressBar progressBar;


    #region Unity Methods
    void Start()
    {
        progressBar = GetComponent<ProgressBar>();

        if (stat)
        {
            stat.OnChange += OnStatChange;
            UpdateUI();
        }
    }

    private void OnDestroy()
    {
        if (stat)
            stat.OnChange = OnStatChange;
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Update the player stat progress bar
    /// </summary>
    /// <param name="delta">Amount of change in stat</param>
    /// <param name="value">New stat value</param>
    private void OnStatChange(float delta, float value)
    {
        UpdateUI();
    }

    /// <summary>
    /// Update the UI
    /// </summary>
    private void UpdateUI()
    {
        progressBar.Maximum = stat.Maximum;
        progressBar.Minimum = stat.Minimum;
        progressBar.SetProgress(stat.Value);
    }
    #endregion
}
