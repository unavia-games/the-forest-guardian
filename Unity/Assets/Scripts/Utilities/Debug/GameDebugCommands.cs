﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using Unavia.TheForestGuardian.Behaviours;
using UnityEngine;


namespace Unavia.TheForestGuardian.Utilities
{
    /// <summary>
    /// Debug commands for the game
    /// </summary>
    public class GameDebugCommands : MonoBehaviour, IDebugCommands
    {
        #region Properties
        [SerializeField]
        [Required]
        private PlayerController playerController;
        [SerializeField]
        [Required]
        private ForestManager forestController;
        #endregion

        private Dictionary<string, StatBehaviour> playerStats;
        private Dictionary<string, StatBehaviour> forestStats;
        private HealthStat playerHealth;
        private StaminaStat playerStamina;
        private ForestTreesStat forestTrees;
        private ForestWildlifeStat forestWildlife;
        private ForestFoodStat forestFood;

        #region Unity Methods
        private void Start()
        {
            playerHealth = playerController.GetComponent<HealthStat>();
            playerStamina = playerController.GetComponent<StaminaStat>();
            forestTrees = forestController.GetComponent<ForestTreesStat>();
            forestWildlife = forestController.GetComponent<ForestWildlifeStat>(); ;
            forestFood = forestController.GetComponent<ForestFoodStat>();

            playerStats = new Dictionary<string, StatBehaviour>
            {
                { "health", playerHealth },
                { "stamina", playerStamina }
            };

            forestStats = new Dictionary<string, StatBehaviour>
            {
                { "trees", forestTrees },
                { "wildlife", forestWildlife },
                { "food", forestFood },
            };
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Register debug commands with the debug console singleton
        /// </summary>
        /// <param name="console">Debug console reference</param>
        public void RegisterCommands(DebugConsoleManager console)
        {
            RegisterForestCommands(console);
            RegisterPlayerCommands(console);

            console.AddCommand(
                new DebugCommand<bool>("godmode_set", "Enable player godmode (disables damage, survival, etc)", "godmode_set <bool>",
                (bool value) =>
                {
                    return new DebugCommandResult(true, $"Setting god mode to {value}");
                }));

            console.AddCommand(
                new DebugCommand<int>("day_set", "Set game day", "day_set <number>",
                (int value) =>
                {
                    return new DebugCommandResult(true, $"Setting day to {value}");
                }));


            console.AddCommand(
                new DebugCommand<string, int>("item_give", "Give player an amount of an item", "item_give <item> <amount>",
                (string item, int amount) =>
                {
                    return new DebugCommandResult(true, $"Gave player {item} ({amount})");
                }));
        }

        private void RegisterPlayerCommands(DebugConsoleManager console)
        {
            console.AddCommand(
                new DebugCommand<string, int>("player_stat_decrease", "Decrease a player stat value [health, stamina]", "player_stat_decrease <stat> <value>",
                (string statName, int value) =>
                {
                    if (!playerStats.TryGetValue(statName, out StatBehaviour stat))
                        return DebugCommandResult.InvalidArgs;

                    stat.Decrease(value);

                    return new DebugCommandResult(true, $"Decreasing player {statName} by {value} to {stat.Value}");
                }));

            console.AddCommand(
                new DebugCommand<string, int>("player_stat_increase", "Increase a player stat value [health, stamina]", "player_stat_increase <stat> <value>",
                (string statName, int value) =>
                {
                    if (!playerStats.TryGetValue(statName, out StatBehaviour stat))
                        return DebugCommandResult.InvalidArgs;

                    stat.Increase(value);

                    return new DebugCommandResult(true, $"Increasing player {statName} by {value}");
                }));

            console.AddCommand(
                new DebugCommand<string, int>("player_stat_set", "Set a player stat value [health, stamina]", "player_stat_set <stat> <value>",
                (string statName, int value) =>
                {
                    if (!playerStats.TryGetValue(statName, out StatBehaviour stat))
                        return DebugCommandResult.InvalidArgs;

                    bool mustDecrease = stat.Value > value;
                    if (mustDecrease)
                        stat.Decrease(stat.Value - value);
                    else
                        stat.Increase(stat.Value - value);

                    return new DebugCommandResult(true, $"Setting player {statName} to {value}");
                }));
        }

        private void RegisterForestCommands(DebugConsoleManager console)
        {
            console.AddCommand(
                new DebugCommand<string, int>("forest_stat_decrease", "Decrease a forest stat value [trees, wildlife, food]", "forest_stat_decrease <stat> <value>",
                (string statName, int value) =>
                {
                    if (!forestStats.TryGetValue(statName, out StatBehaviour stat))
                        return DebugCommandResult.InvalidArgs;

                    stat.Decrease(value);

                    return new DebugCommandResult(true, $"Decreasing forest {statName} by {value} to {stat.Value}");
                }));

            console.AddCommand(
                new DebugCommand<string, int>("forest_stat_increase", "Increase a forest stat value [trees, wildlife, food]", "forest_stat_increase <stat> <value>",
                (string statName, int value) =>
                {
                    if (!forestStats.TryGetValue(statName, out StatBehaviour stat))
                        return DebugCommandResult.InvalidArgs;

                    stat.Increase(value);

                    return new DebugCommandResult(true, $"Increasing forest {statName} by {value}");
                }));

            console.AddCommand(
                new DebugCommand<string, int>("forest_stat_set", "Set a forest stat value [trees, wildlife, food]", "forest_stat_set <stat> <value>",
                (string statName, int value) =>
                {
                    if (!forestStats.TryGetValue(statName, out StatBehaviour stat))
                        return DebugCommandResult.InvalidArgs;

                    bool mustDecrease = stat.Value > value;
                    if (mustDecrease)
                        stat.Decrease(stat.Value - value);
                    else
                        stat.Increase(stat.Value - value);

                    return new DebugCommandResult(true, $"Setting forest {statName} to {value}");
                }));
        }
        #endregion
    }
}
