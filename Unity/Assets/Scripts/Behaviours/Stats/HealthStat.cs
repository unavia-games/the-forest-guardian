﻿using System;
using Unavia.TheForestGuardian.Behaviours;
using UnityEngine;

/// <summary>
/// Health statistic
/// </summary>
public class HealthStat : StatBehaviour
{
    #region Properties
    /// <summary>
    /// Whether the GameObject should be destroyed on death
    /// </summary>
    private bool destroyOnDeath = true;

    public bool Alive { get; private set; }
    #endregion


    #region Events
    /// <summary>
    /// Invoked when entity receives damada
    /// </summary>
    /// </summary>
    [HideInInspector]
    public Action<float, GameObject> OnDamage;
    /// <summary>
    /// Invoked when entity dies from damage
    /// </summary>
    [HideInInspector]
    public Action<GameObject> OnDeath;
    #endregion


    #region Unity Methods
    void Awake()
    {
        Alive = true;
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Immediately cause entity death
    /// </summary>
    /// <param name="killer">Object causing death</param>
    public void Kill(GameObject killer)
    {
        Die(killer);
    }

    /// <summary>
    /// Take damage from a source
    /// </summary>
    /// <param name="damage">Amount of damage</param>
    /// <param name="damager">Object inflicting damage</param>
    public void TakeDamage(float damage, GameObject damager)
    {
        if (!Alive) return;

        Decrease(damage);

        // Losing remaining health should kill the entity without sending damage event.
        //   Therefore, the "BaseStat.OnMin" action cannot be used.
        if (Value <= 0)
        {
            Die(damager);
            return;
        }

        // Notify entity that damage has been taken
        OnDamage?.Invoke(damage, damager);
    }

    /// <summary>
    /// Kill the damageable entity
    /// </summary>
    /// <param name="killer">Object causing death</param>
    public void Die(GameObject killer)
    {
        if (!Alive) return;

        Alive = false;

        // Notify entity that it has been killed
        OnDeath?.Invoke(killer);

        if (destroyOnDeath)
            Destroy(gameObject);
    }
    #endregion
}
