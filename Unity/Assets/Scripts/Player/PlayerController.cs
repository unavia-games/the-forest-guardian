﻿using ECM.Common;
using ECM.Controllers;
using UnityEngine;

[RequireComponent(typeof(HealthStat))]
[RequireComponent(typeof(StaminaStat))]
public class PlayerController : BaseCharacterController
{
    #region Properties
    [Header("Custom Controller Fields")]
    /// <summary>
    ///  Reference to player camera
    /// </summary>
    public Transform PlayerCamera;

    [Range(2, 10)]
    public float WalkSpeed = 5f;
    [Range(5, 20)]
    public float RunSpeed = 10f;
    /// <summary>
    /// Stamina drain per second when walking
    /// </summary>
    [Range(0, 5f)]
    public float WalkStaminaDrain = 1f;
    /// <summary>
    /// Stamina drain per second when running
    /// </summary>
    [Range(0, 10f)]
    public float RunStaminaDrain = 2.5f;

    public bool IsWalking { get; private set; }

    private HealthStat health;
    private StaminaStat stamina;
    private InputActions inputActions;
    #endregion


    #region Unity Methods
    public override void Awake()
    {
        base.Awake();

        health = GetComponent<HealthStat>();
        stamina = GetComponent<StaminaStat>();

        animator = GetComponentInChildren<Animator>();
        inputActions = new InputActions();
    }

    public override void Update()
    {
        base.Update();

        // Drain stamina while playing is moving (and grounded)
        if (moveDirection != Vector3.zero && isGrounded)
        {
            // TODO: Extract logic to a better place
            float drain = IsWalking ? WalkStaminaDrain : RunStaminaDrain;
            stamina.DrainStamina(drain * Time.deltaTime);
        }
    }

    public void OnEnable()
    {
        inputActions.Enable();
    }

    public void OnDisable()
    {
        inputActions.Disable();
    }
    #endregion


    #region Custom Methods
    protected override void Animate()
    {
        if (animator == null) return;

        float animatorSpeed = IsWalking
            // ? moveDirection.magnitude * (WalkSpeed / RunSpeed)
            ? moveDirection.magnitude * 0.5f
            : moveDirection.magnitude;

        animator.SetFloat("Speed", animatorSpeed, 0.1f, Time.deltaTime);

        animator.SetBool("OnGround", movement.isGrounded);

        if (!movement.isGrounded)
        {
            animator.SetFloat("Jump", movement.velocity.y, 0.1f, Time.deltaTime);
        }
        else
        {
            animator.SetFloat("Jump", 0f);
        }
    }

    /// <summary>
    /// Override "BaseCharacterController" to handle different speeds
    /// </summary>
    /// <returns>Character velocity</returns>
    protected override Vector3 CalcDesiredVelocity()
    {
        speed = GetTargetSpeed();

        return base.CalcDesiredVelocity();
    }

    /// <summary>
    /// Get target speed based on character state (ie. running, walking, etc)
    /// </summary>
    /// <returns>Target speed from character state</returns>
    private float GetTargetSpeed()
    {
        return IsWalking ? WalkSpeed : RunSpeed;
    }

    /// <summary>
    /// Override "BaseCharacterController" input handling (uses new InputSystem)
    /// </summary>
    protected override void HandleInput()
    {
        // Transform movement direction to be relative to camera view direction
        Vector2 keyboardMovement = inputActions.PlayerMovement.Movement.ReadValue<Vector2>();
        moveDirection = new Vector3 { x = keyboardMovement.x, y = 0f, z = keyboardMovement.y };
        moveDirection = moveDirection.relativeTo(PlayerCamera);

        jump = inputActions.PlayerMovement.Jump.ReadValue<float>() > 0;
        IsWalking = inputActions.PlayerMovement.Run.ReadValue<float>() == 0 || !stamina.HasStamina;
    }
    #endregion
}
