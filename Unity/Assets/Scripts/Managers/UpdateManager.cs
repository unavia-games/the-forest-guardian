﻿using System;
using UnityEngine;

/// <summary>
/// Provide update cycle for non-MonoBehaviour classes
/// </summary>
/// <remarks>This should be used sparingly!</remarks>
/// <remarks>Taken from: https://forum.unity.com/threads/how-to-call-update-from-a-class-thats-not-inheriting-from-monobehaviour.451954/#post-2926769</remarks>
//public class UpdateManager : GameSingleton<UpdateManager>
public class UpdateManager : MonoBehaviour
{
    #region Events
    private static UpdateManager instance = null;

    /// <summary>
    /// Raised during the Unity event loop. Itended for use by scripts not able to inherit from MonoBehaviour.
    /// </summary>
    public static Action OnUpdate;
    #endregion


    #region Unity Methods
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else if (this != instance)
        {
            Destroy(this);
        }
    }

    /// <summary>
    /// Invoke all subscribed listeners every frame
    /// </summary>
    void Update()
    {
        OnUpdate?.Invoke();
    }
    #endregion
}
