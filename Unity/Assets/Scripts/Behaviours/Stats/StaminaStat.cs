﻿using System;
using Unavia.TheForestGuardian.Behaviours;
using UnityEngine;

/// <summary>
/// Stamina statistic
/// </summary>
public class StaminaStat : RegenStatBehaviour
{
	#region Properties
    /// <summary>
    /// Whether the entity has stamina
    /// </summary>
    public bool HasStamina { get => !IsMin; }
    #endregion


    #region Events
    /// <summary>
    /// Invoked when stamina is completely drained
    /// </summary>
    [HideInInspector]
    public Action OnDrain;
    #endregion


    #region Unity Methods
    void Awake()
    {
        // QUESTION: Does this work???
        OnDrain = OnMinValue;
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Drain user stamina
    /// </summary>
    /// <param name="amount">Stamina drain amount</param>
    public void DrainStamina(float amount)
    {
        Decrease(amount);
    }
    #endregion
}
