﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimeScale : MonoBehaviour
{
    #region Properties
    [Header("Scale")]
    /// <summary>
    /// Timescale value
    /// </summary>
    [SerializeField]
    private float timeScale = 1f;

    [Header("UI Elements")]
    /// <summary>
    /// Timescale UI panel
    /// </summary>
    [SerializeField]
    private GameObject panelUI;
    /// <summary>
    /// Whether the panel should be shown
    /// </summary>
    [SerializeField]
    private bool showPanel;
    #endregion

    private TextMeshProUGUI scaleText;
    private Slider scaleSlider;

    // Several values are cached to avoid constantly setting UI values
    //   in response to Inspector changes (since we cannot listen to events).
    private float previousTimeScale;
    private bool previousShown;


    #region Unity Methods
    void Awake()
    {
        // UI elements are optional!
        if (panelUI != null)
        {
            scaleText = panelUI.GetComponentInChildren<TextMeshProUGUI>();
            scaleSlider = panelUI.GetComponentInChildren<Slider>();

            if (scaleSlider != null)
            {
                scaleSlider.onValueChanged.AddListener(UpdateTimeScale);
            }
        }

        UpdateTimeScale(Time.timeScale);
    }

    private void OnValidate()
    {
        // Handles changes done via the Inspector (handler responds to UI)
        UpdateTimeScale(timeScale);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Update the timescale value
    /// </summary>
    /// <param name="value">New timescale value</param>
    public void UpdateTimeScale(float value)
    {
        previousTimeScale = timeScale;
        timeScale = Mathf.Clamp01(value);

        Time.timeScale = timeScale;

        UpdateTimeScaleUI();
    }

    /// <summary>
    /// Update the timescale value UI
    /// </summary>
    private void UpdateTimeScaleUI()
    {
        if (panelUI != null)
        {
            panelUI.SetActive(showPanel);
        }

        if (scaleSlider != null)
        {
            scaleSlider.value = timeScale;
        }

        if (scaleText != null)
        {
            scaleText.text = $"{timeScale:F1}";
        }
    }
    #endregion
}
