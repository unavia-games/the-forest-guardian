﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Unavia.TheForestGuardian.Behaviours
{
    /// <summary>
    /// Base regenerable stat behaviour
    /// </summary>
    public abstract class RegenStatBehaviour : StatBehaviour
    {
        #region Properties
        /// <summary>
        /// Whether the stat can regenerate
        /// </summary>
        [SerializeField]
        protected bool canRegenerate = false;
        /// <summary>
        /// Regeneration rate per second
        /// </summary>
        [SerializeField]
        [ShowIf("canRegenerate")]
        protected float regenRate = 0f;
        /// <summary>
        /// Delay since last change before regeneration can start
        /// </summary>
        [SerializeField]
        [ShowIf("canRegenerate")]
        protected float regenDelay = 0f;
        #endregion

        /// <summary>
        /// When the stat can start regenerating again
        /// </summary>
        protected float? startRegenTime = null;


        #region Unity Methods
        private void Update()
        {
            // Regeneration can only occur after waiting the delay period from the last change
            if (!canRegenerate || !startRegenTime.HasValue || startRegenTime.Value > Time.time) return;

            // Continue regenerating stat until maximum threshold is reached
            if (!IsMax)
            {
                Increase(regenRate * Time.deltaTime);
                return;
            }

            startRegenTime = null;
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Change whether the stat can regenerate
        /// </summary>
        /// <param name="canRegenerate">Whether the stat can regenerate</param>
        /// <param name="mustDelay">Whether the stat must delay before regenerating</param>
        public void ToggleRegeneration(bool canRegenerate, bool mustDelay = false)
        {
            this.canRegenerate = canRegenerate;

            // Toggling regeneration may require waiting for normal delay to start again
            if (mustDelay)
            {
                ResetRegenTime();
            }
        }

        /// <summary>
        /// Reset when regeneration can start (uses delay from the last stat change)
        /// </summary>
        public void ResetRegenTime()
        {
            startRegenTime = Time.time + regenDelay;
        }

        /// <summary>
        /// Decrease the stat by an amount
        /// </summary>
        /// <param name="value">Amount to decrease</param>
        public override void Decrease(float value)
        {
            base.Decrease(value);

            ResetRegenTime();
        }
        #endregion
    }
}
