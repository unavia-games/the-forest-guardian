﻿using System;


namespace Unavia.TheForestGuardian.Utilities
{
    /// <summary>
    /// Base console debug command
    /// </summary>
    public class DebugCommandBase
    {
        /// <summary>
        /// Command name/code
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Command description
        /// </summary>
        public string Description { get; private set; }
        /// <summary>
        /// Command format (for user)
        /// </summary>
        public string Format { get; private set; }

        public DebugCommandBase(string name, string description, string format)
        {
            Name = name;
            Description = description;
            Format = format;
        }
    }


    /// <summary>
    /// Debug command with no parameters
    /// </summary>
    public class DebugCommand : DebugCommandBase
    {
        public Func<DebugCommandResult> Command { get; private set; }

        public DebugCommand(string name, string description, string format, Func<DebugCommandResult> command) : base(name, description, format)
        {
            Command = command;
        }

        public DebugCommandResult Invoke()
        {
            return Command.Invoke();
        }
    }


    /// <summary>
    /// Debug command with a single parameter
    /// </summary>
    /// <typeparam name="T1">First parameter</typeparam>
    public class DebugCommand<T1> : DebugCommandBase
    {
        public Func<T1, DebugCommandResult> Command { get; private set; }

        public DebugCommand(string name, string description, string format, Func<T1, DebugCommandResult> command) : base(name, description, format)
        {
            Command = command;
        }

        public DebugCommandResult Invoke(T1 value)
        {
            return Command.Invoke(value);
        }
    }


    /// <summary>
    /// Debug command with a second parameter
    /// </summary>
    /// <typeparam name="T1">First parameter</typeparam>
    /// <typeparam name="T2">Second parameter</typeparam>
    public class DebugCommand<T1, T2> : DebugCommandBase
    {
        public Func<T1, T2, DebugCommandResult> Command { get; private set; }

        public DebugCommand(string name, string description, string format, Func<T1, T2, DebugCommandResult> command) : base(name, description, format)
        {
            Command = command;
        }

        public DebugCommandResult Invoke(T1 subCommand, T2 value)
        {
            return Command.Invoke(subCommand, value);
        }
    }
}
